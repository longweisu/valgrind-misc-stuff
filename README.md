

Valgrind
====
* build valgrind from 3.10.1 
* default.supp file generated from configure
* debian.supp from ubuntu valgrind package
* kde.supp from some random helgrind blogpost
* creaate a .valgrindrc in your ~

drawback
---
memory issue only logged with normal exit.

The problem with valgrind with hipchat is that it is relative slow, the overhead of computation is about 20~50, the memory overhead is about 2 to 1. for a multi-thread application, valgrind serilize the code execution. 


TODO
===

heaptracker

AddressSanitizer



Heaptracker
===

It is a heap memory profiler. it is a tool to decide which part of program are responsible for excessive heap memory consumption.
This is a faster alternative of massif.
maybe we should use http://goog-perftools.sourceforge.net/doc/heap_profiler.html as more mature alternative.

the trick is using LD_PRELOAD, that DSO will loaded before any library(include c runtime). so libheaptrack_preload and libheaptrack_inject.so are injected into debuggee application. they overload malloc & friends, grab backtrack using libunwind + libbacktrace.


```
heaptrack ./foo

heaptrack_print "/home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/heaptrack.test.3121.gz" -M  test.out -l 1

massif-visualizer test.out 
```

it don't support supp file, which is annoying because libc and glib throw ton of false positive message.

but this tool failed to detect a simple memory leak. maybe I configure it wrong

AddressSanitizer
====
this slow application be a factor of 2, at the cost of not detecting errors like uninitilized variables or leak. (which we really don't care for now)

add this to pro file

```
QMAKE_CXXFLAGS+="-fsanitize=address -fno-omit-frame-pointer"
QMAKE_CFLAGS+="-fsanitize=address -fno-omit-frame-pointer"
QMAKE_LFLAGS+="-fsanitize=address"
```

or cmakelists

```
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11 -fsanitize=address -fno-omit-frame-pointer" )
    set(CMAKE_CF_FLAGS "${CMAKE_CF_FLAGS} -fsanitize=address -fno-omit-frame-pointer" )
    set(CMAKE_LF_FLAGS "${CMAKE_LF_FLAGS} -fsanitize=address" )
```


```
./foo 2>&1 | asan_symbolize.py | c++filt

```

example output

```
longwei@ubuntu:~/build-test-Desktop_Qt_5_3_GCC_64bit-Debug$ ./test 2>&1 | ./asan_symbolize.py | c++filt 
=================================================================
==15097== ERROR: AddressSanitizer: heap-use-after-free on address 0x6006000e5120 at pc 0x403942 bp 0x7fff2bb3d9f0 sp 0x7fff2bb3d9e8
READ of size 8 at 0x6006000e5120 thread T0
    #0 0x403941 in MainWindow::on_pushButton_clicked() /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/../test/mainwindow.cpp:25
    #1 0x4048ae in MainWindow::qt_static_metacall(QObject*, QMetaObject::Call, int, void**) /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/moc_mainwindow.cpp:67
    #2 0x4049ca in MainWindow::qt_metacall(QMetaObject::Call, int, void**) /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/moc_mainwindow.cpp:100
    #3 0x7fa473831d92 in QMetaObject::activate(QObject*, int, int, void**) ??:?
    #4 0x7fa474140c71 in QAbstractButton::clicked(bool) ??:?
    #5 0x7fa473eadfb2 in QAbstractButton::event(QEvent*) ??:?
    #6 0x7fa473eae8c9 in QAbstractButton::keyPressEvent(QKeyEvent*) ??:?
    #7 0x7fa473eaead3 in QAbstractButton::mouseReleaseEvent(QMouseEvent*) ??:?
    #8 0x7fa473de68e6 in QWidget::event(QEvent*) ??:?
    #9 0x7fa473dae763 in QApplicationPrivate::notify_helper(QObject*, QEvent*) ??:?
    #10 0x7fa473db1b5b in QApplication::notify(QObject*, QEvent*) ??:?
    #11 0x7fa473800153 in QCoreApplication::notifyInternal(QObject*, QEvent*) ??:?
    #12 0x7fa473db0cc9 in QApplicationPrivate::sendMouseEvent(QWidget*, QMouseEvent*, QWidget*, QWidget*, QWidget**, QPointer<QWidget>&, bool) ??:?
    #13 0x7fa473e06b56 in QWidgetPrivate::create_sys(unsigned long long, bool, bool) ??:?
    #14 0x7fa473e096a7 in QWidgetPrivate::create_sys(unsigned long long, bool, bool) ??:?
    #15 0x7fa473dae763 in QApplicationPrivate::notify_helper(QObject*, QEvent*) ??:?
    #16 0x7fa473db1d85 in QApplication::notify(QObject*, QEvent*) ??:?
    #17 0x7fa473800153 in QCoreApplication::notifyInternal(QObject*, QEvent*) ??:?
    #18 0x7fa4721f2bf6 in QGuiApplicationPrivate::processMouseEvent(QWindowSystemInterfacePrivate::MouseEvent*) ??:?
    #19 0x7fa4721f35f4 in QGuiApplicationPrivate::processWindowSystemEvent(QWindowSystemInterfacePrivate::WindowSystemEvent*) ??:?
    #20 0x7fa4721d85b7 in QWindowSystemInterface::sendWindowSystemEvents(QFlags<QEventLoop::ProcessEventsFlag>) ??:?
    #21 0x7fa46c095a6f in xcb_aux_clear_window ??:?
    #22 0x7fa471bf9e03 in g_main_context_dispatch ??:?
    #23 0x7fa471bfa047 in g_main_context_dispatch ??:?
    #24 0x7fa471bfa0eb in g_main_context_iteration ??:?
    #25 0x7fa47385a823 in QEventDispatcherGlib::processEvents(QFlags<QEventLoop::ProcessEventsFlag>) ??:?
    #26 0x7fa4737fe36a in QEventLoop::exec(QFlags<QEventLoop::ProcessEventsFlag>) ??:?
    #27 0x7fa473803234 in QCoreApplication::exec() ??:?
    #28 0x4034da in main /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/../test/main.cpp:10
    #29 0x7fa472c72ec4 in __libc_start_main /build/buildd/eglibc-2.19/csu/libc-start.c:287
    #30 0x403378 in _start ??:?
0x6006000e5120 is located 0 bytes inside of 32-byte region [0x6006000e5120,0x6006000e5140)
freed by thread T0 here:
    #0 0x7fa4744c09da in operator delete(void*) ??:?
    #1 0x4038e7 in MainWindow::on_pushButton_clicked() /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/../test/mainwindow.cpp:24 (discriminator 1)
    #2 0x4048ae in MainWindow::qt_static_metacall(QObject*, QMetaObject::Call, int, void**) /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/moc_mainwindow.cpp:67
    #3 0x4049ca in MainWindow::qt_metacall(QMetaObject::Call, int, void**) /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/moc_mainwindow.cpp:100
    #4 0x7fa473831d92 in QMetaObject::activate(QObject*, int, int, void**) ??:?
previously allocated by thread T0 here:
    #0 0x7fa4744c081a in operator new(unsigned long) ??:?
    #1 0x403684 in MainWindow::MainWindow(QWidget*) /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/../test/mainwindow.cpp:10
    #2 0x4034c9 in main /home/longwei/build-test-Desktop_Qt_5_3_GCC_64bit-Debug/../test/main.cpp:7
    #3 0x7fa472c72ec4 in __libc_start_main /build/buildd/eglibc-2.19/csu/libc-start.c:287
Shadow bytes around the buggy address:
  0x0c01400149d0: fd fd fd fd fa fa fd fd fd fd fa fa fd fd fd fd
  0x0c01400149e0: fa fa fd fd fd fa fa fa fd fd fd fd fa fa 00 00
  0x0c01400149f0: 00 00 fa fa fd fd fd fa fa fa fd fd fd fd fa fa
  0x0c0140014a00: fd fd fd fa fa fa 00 00 00 00 fa fa 00 00 00 00
  0x0c0140014a10: fa fa 00 00 00 00 fa fa fd fd fd fa fa fa fd fd
=>0x0c0140014a20: fd fa fa fa[fd]fd fd fd fa fa 00 00 00 00 fa fa
  0x0c0140014a30: fd fd fd fa fa fa fd fd fd fd fa fa fd fd fd fa
  0x0c0140014a40: fa fa fd fd fd fa fa fa fd fd fd fa fa fa fd fd
  0x0c0140014a50: fd fa fa fa fd fd fd fa fa fa fd fd fd fa fa fa
  0x0c0140014a60: fd fd fd fa fa fa fd fd fd fa fa fa fd fd fd fa
  0x0c0140014a70: fa fa fd fd fd fa fa fa fd fd fd fa fa fa fd fd
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07
  Heap left redzone:     fa
  Heap righ redzone:     fb
  Freed Heap region:     fd
  Stack left redzone:    f1
  Stack mid redzone:     f2
  Stack right redzone:   f3
  Stack partial redzone: f4
  Stack after return:    f5
  Stack use after scope: f8
  Global redzone:        f9
  Global init order:     f6
  Poisoned by user:      f7
  ASan internal:         fe
==15097== ABORTING
```

