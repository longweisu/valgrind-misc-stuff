#ifndef NODEWIDGET_H
#define NODEWIDGET_H

#include <QWidget>

class NodeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit NodeWidget(QWidget *parent = 0);
    ~NodeWidget();

signals:

public slots:
};

#endif // NODEWIDGET_H
