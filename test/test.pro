#-------------------------------------------------
#
# Project created by QtCreator 2015-02-12T09:56:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    nodewidget.cpp

HEADERS  += mainwindow.h \
    nodewidget.h

FORMS    += mainwindow.ui
